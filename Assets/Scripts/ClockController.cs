using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ClockController : MonoBehaviour, IPointerClickHandler
{
    [SerializeField]
    GameObject secondsHand;

    [SerializeField]
    GameObject secondsActualHand;

    [SerializeField]
    GameObject minutesHand;

    [SerializeField]
    GameObject hoursHand;

    public GameObject textObj;
    public GameObject sphere;
    public Transform originalTransform;

    Rigidbody rb;
    DateTime time;
    public static MeshRenderer sHandRenderer;
    public Transform imageTargetTransform;

    float secondsDegree;
    float minutesDegree;
    float hoursDegree;

    public static int hours;
    public static bool check = true;
    public static bool engLanguage = true;
    public static string currentZone;
    public static string[] gmtZones;
    public static Color[] colors = { Color.red, Color.yellow, Color.green, Color.blue, Color.magenta, Color.cyan};
    public static int currentZoneID;
    int currentDay;

    Vector3 V1;

    void Start()
    {
        gmtZones = new string[25];
        gmtZones[0] = "UTC(0)";
        gmtZones[1] = "UTC+1";
        gmtZones[2] = "UTC+2";
        gmtZones[3] = "UTC+3";
        gmtZones[4] = "UTC+4";
        gmtZones[5] = "UTC+5";
        gmtZones[6] = "UTC+6";
        gmtZones[7] = "UTC+7";
        gmtZones[8] = "UTC+8";
        gmtZones[9] = "UTC+9";
        gmtZones[10] = "UTC+10";
        gmtZones[11] = "UTC+11";
        gmtZones[12] = "UTC+12";
        gmtZones[13] = "UTC-11";
        gmtZones[14] = "UTC-10";
        gmtZones[15] = "UTC-9";
        gmtZones[16] = "UTC-8";
        gmtZones[17] = "UTC-7";
        gmtZones[18] = "UTC-6";
        gmtZones[19] = "UTC-5";
        gmtZones[20] = "UTC-4";
        gmtZones[21] = "UTC-3";
        gmtZones[22] = "UTC-2";
        gmtZones[23] = "UTC-1";

        currentZoneID = 0;
        currentZone = gmtZones[currentZoneID];
        int day = (int)DateTime.Now.DayOfWeek;

        sHandRenderer = secondsActualHand.GetComponent<MeshRenderer>();

        V1 = new Vector3();
    }

    void Update()
    {
		transform.Rotate(new Vector3(0f, 0.3f, 0f));
		time = DateTime.Now;

        currentDay = (int)DateTime.Now.DayOfWeek;
        var textMesh = textObj.GetComponent<TextMesh>();

        if (engLanguage)
        {
            switch (currentDay)
            {
                case 0:
                    textMesh.text = "Sun";
                    break;
                case 1:
                    textMesh.text = "Mon";
                    break;
                case 2:
                    textMesh.text = "Tue";
                    break;
                case 3:
                    textMesh.text = "Wen";
                    break;
                case 4:
                    textMesh.text = "Thu";
                    break;
                case 5:
                    textMesh.text = "Fri";
                    break;
                case 6:
                    textMesh.text = "Sat";
                    break;
            }
        }

        if (engLanguage == false)
        {
            switch (currentDay)
            {
                case 0:
                    textMesh.text = "Вос";
                    break;
                case 1:
                    textMesh.text = "Пон";
                    break;
                case 2:
                    textMesh.text = "Вто";
                    break;
                case 3:
                    textMesh.text = "Сре";
                    break;
                case 4:
                    textMesh.text = "Чет";
                    break;
                case 5:
                    textMesh.text = "Пят";
                    break;
                case 6:
                    textMesh.text = "Суб";
                    break;
            }
        }
        if (check)
        {
            hours = DateTime.Now.Hour;
        }

        secondsDegree = -(time.Second / 60f) * 360f;
        V1.Set(secondsDegree, 0, 0);
		secondsHand.transform.localRotation = Quaternion.Euler(V1);
         
		minutesDegree = -(time.Minute / 60f) * 360f;
        V1.Set(minutesDegree, 0, 0);
		minutesHand.transform.localRotation = Quaternion.Euler(V1);

		hoursDegree = -(hours / 12f) * 360f;
        V1.Set(hoursDegree, 0, 0);
		hoursHand.transform.localRotation = Quaternion.Euler(V1);
    }

	public void OnPointerClick(PointerEventData pointerEventData)
	{
        GameObject newSphere = Instantiate(sphere, new Vector3(originalTransform.position.x, originalTransform.position.y + 1.5f, originalTransform.position.z), Quaternion.identity);
        newSphere.AddComponent<Rigidbody>();
        newSphere.transform.parent = imageTargetTransform;
        rb = newSphere.GetComponent<Rigidbody>();
        rb.AddForce(UnityEngine.Random.Range(-400f, 400f), 1000, UnityEngine.Random.Range(-400f, 400f), ForceMode.Force);
    }
}
