﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ScreenShot : MonoBehaviour, IPointerClickHandler
{
    int scrShotIncrementer = 0;

    public void OnPointerClick(PointerEventData pointerEventData)
    {
        ScreenCapture.CaptureScreenshot("Bully!ScreenShot" + scrShotIncrementer);
        scrShotIncrementer += 1;
    }
}
