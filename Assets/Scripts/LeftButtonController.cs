﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class LeftButtonController : MonoBehaviour, IPointerClickHandler
{
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log(ClockController.currentZoneID);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnPointerClick(PointerEventData pointerEventData)
    {
        ClockController.hours -= 1;
        ClockController.check = false;
        ClockController.engLanguage = !ClockController.engLanguage;
        
        ClockController.sHandRenderer.material.color = ClockController.colors[UnityEngine.Random.Range(0, 5)];
        if (ClockController.currentZoneID != 0)
        {
            ClockController.currentZoneID -= 1;
            ClockController.currentZone = ClockController.gmtZones[ClockController.currentZoneID];

        }

        else 
        {
            ClockController.currentZone = ClockController.gmtZones[23];
            ClockController.currentZoneID = 23;
        }
        Debug.Log(ClockController.currentZone);
    }
}

