﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;

public class ButtonController : MonoBehaviour,IPointerClickHandler
{

    public void OnPointerClick(PointerEventData pointerEventData)
	{
        ClockController.hours += 1;
        ClockController.check = false;
        ClockController.engLanguage = !ClockController.engLanguage;

        if (ClockController.currentZone == "UTC-1" && ClockController.currentZoneID == 23)
        {
            ClockController.currentZone = ClockController.gmtZones[0];
            ClockController.currentZoneID = -1;
        }

        ClockController.currentZone = ClockController.gmtZones[ClockController.currentZoneID + 1];
        ClockController.currentZoneID += 1;
        ClockController.sHandRenderer.material.color = ClockController.colors[UnityEngine.Random.Range(0, 5)];
        Debug.Log(ClockController.currentZone);
	}
}
